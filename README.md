# Cable Modem Stats

Cable Modem Stats is a Python library for getting useful statistics from various cable modems. It is currently in beta.

## Example
```py
from cable_modem_stats import connect

modem = connect('192.168.100.1')
print("Get all downstreams:")
for ds in modem.downstreams:
    print(ds)

print("Get all upstreams")
for us in modem.upstreams:
    print(us)
```

## Requires
 * [beautifulsoup4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
 * [requests](https://github.com/kennethreitz/requests)
 * [six](https://pythonhosted.org/six/)
 * Python 2.7 or 3.3+

## Extending
Subclass the `BaseModem` in the `cable_modem_stats.models` package and fill in the fields with the appropriate logic.

## License
[Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)
